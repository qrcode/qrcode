﻿using UnityEngine;
using System.Collections;

public class WebCamScript : MonoBehaviour {


    private WebCamDevice[] webCamDevices;
    private Renderer webCamRenderer;

    void Awake()
    {
        webCamDevices = WebCamTexture.devices;
    }

    void Start () {
        // for debugging
        foreach (var device in webCamDevices) {
            Debug.Log("device = " + device.name);
        }

        // Renderer
        webCamRenderer = GetComponent<Renderer>();

        if (webCamDevices.Length > 0)
        {
            // WebCamTexture webcam = new WebCamTexture("Integrated Webcam");
            WebCamTexture webcam = new WebCamTexture(webCamDevices[0].name);
            webCamRenderer.material.mainTexture = webcam;

            webcam.Play();
        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}
