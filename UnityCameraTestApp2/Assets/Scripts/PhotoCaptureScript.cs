﻿using UnityEngine;
using System.Collections;
using UnityEngine.VR.WSA.WebCam;
using System.Linq;
using System.IO;
using System.Collections.Generic;

public class PhotoCaptureScript : MonoBehaviour {


// #if !UNITY_EDITOR
// #endif

    PhotoCapture photoCaptureObject = null;
    //bool haveFolderPath = false;
    //StorageFolder picturesFolder;
    //string tempFilePathAndName;
    //string tempFileName;

    // Use this for initialization
    void Start()
    {
        //getFolderPath();
        //while (!haveFolderPath)
        //{
        //    Debug.Log("Waiting for folder path...");
        //}

        Debug.Log("About to call PhotoCapture.CreateAsync()");
        PhotoCapture.CreateAsync(false, OnPhotoCaptureCreated);
        Debug.Log("Called PhotoCapture.CreateAsync()");
    }

    //async void getFolderPath()
    //{
    //    StorageLibrary myPictures = await Windows.Storage.StorageLibrary.GetLibraryAsync(Windows.Storage.KnownLibraryId.Pictures);
    //    picturesFolder = myPictures.SaveFolder;

    //    foreach(StorageFolder fodler in myPictures.Folders)
    //    {
    //        Debug.Log(fodler.Name);

    //    }

    //    Debug.Log("savePicturesFolder.Path is " + picturesFolder.Path);
    //    haveFolderPath = true;
    //}

    void OnPhotoCaptureCreated(PhotoCapture captureObject)
    {
        photoCaptureObject = captureObject;

        Resolution cameraResolution = PhotoCapture.SupportedResolutions.OrderByDescending((res) => res.width * res.height).First();

        CameraParameters c = new CameraParameters();
        c.hologramOpacity = 0.0f;
        c.cameraResolutionWidth = cameraResolution.width;
        c.cameraResolutionHeight = cameraResolution.height;
        c.pixelFormat = CapturePixelFormat.BGRA32;

        captureObject.StartPhotoModeAsync(c, false, OnPhotoModeStarted);
    }

    void OnStoppedPhotoMode(PhotoCapture.PhotoCaptureResult result)
    {
        photoCaptureObject.Dispose();
        photoCaptureObject = null;
    }



    private void OnPhotoModeStarted(PhotoCapture.PhotoCaptureResult result)
    {
        if (result.success)
        {
            try {
                //tempFileName = string.Format(@"CapturedImage{0}_n.jpg", Time.time);
                //string filePath = System.IO.Path.Combine(Application.persistentDataPath, tempFileName);
                //tempFilePathAndName = filePath;

                string filename = string.Format(@"CapturedImage{0}_n.jpg", Time.time);
                string filePath = System.IO.Path.Combine(Application.persistentDataPath, filename);
                Debug.Log("Saving photo to " + filePath);

                // photoCaptureObject.TakePhotoAsync(filePath, PhotoCaptureFileOutputFormat.JPG, OnCapturedPhotoToDisk);

                photoCaptureObject.TakePhotoAsync(OnCapturedPhotoToMemory1);
                // photoCaptureObject.TakePhotoAsync(OnCapturedPhotoToMemory2);

            } catch (System.ArgumentException e)
            {
                Debug.LogError("System.ArgumentException:\n" + e.Message);
            }
        }
        else
        {
            Debug.LogError("Unable to start photo mode!");
        }
    }

    void OnCapturedPhotoToDisk(PhotoCapture.PhotoCaptureResult result)
    {
        if (result.success)
        {
            Debug.Log("Saved Photo to disk!");
            photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);

            //Debug.Log("moving "+tempFilePathAndName+" to " + picturesFolder.Path + "\\Camera Roll\\" + tempFileName);
            //File.Move(tempFilePathAndName, picturesFolder.Path + "\\Camera Roll\\" + tempFileName);
        }
        else
        {
            Debug.Log("Failed to save Photo to disk " +result.hResult+" "+result.resultType.ToString());
        }
    }

    void OnCapturedPhotoToMemory1(PhotoCapture.PhotoCaptureResult result, PhotoCaptureFrame photoCaptureFrame)
    {
        if (result.success) {
            // Create our Texture2D for use and set the correct resolution
            Resolution cameraResolution = PhotoCapture.SupportedResolutions.OrderByDescending((res) => res.width * res.height).First();
            Texture2D targetTexture = new Texture2D(cameraResolution.width, cameraResolution.height);
            // Copy the raw image data into our target texture
            photoCaptureFrame.UploadImageDataToTexture(targetTexture);
            // Do as we wish with the texture such as apply it to a material, etc.

            Debug.Log("Photo saved to memory.");
        } else {
            Debug.LogError("Failed to save the photo to memory.");
        }

        // Clean up
        photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);
    }

    void OnCapturedPhotoToMemory2(PhotoCapture.PhotoCaptureResult result, PhotoCaptureFrame photoCaptureFrame)
    {
        if (result.success) {
            List<byte> imageBufferList = new List<byte>();
            // Copy the raw IMFMediaBuffer data into our empty byte list.
            photoCaptureFrame.CopyRawImageDataIntoBuffer(imageBufferList);

            // In this example, we captured the image using the BGRA32 format.
            // So our stride will be 4 since we have a byte for each rgba channel.
            // The raw image data will also be flipped so we access our pixel data
            // in the reverse order.
            int stride = 4;
            float denominator = 1.0f / 255.0f;
            List<Color> colorArray = new List<Color>();
            for (int i = imageBufferList.Count - 1; i >= 0; i -= stride) {
                float a = (int)(imageBufferList[i - 0]) * denominator;
                float r = (int)(imageBufferList[i - 1]) * denominator;
                float g = (int)(imageBufferList[i - 2]) * denominator;
                float b = (int)(imageBufferList[i - 3]) * denominator;

                colorArray.Add(new Color(r, g, b, a));
            }
            // Now we could do something with the array such as texture.SetPixels() or run image processing on the list

            Debug.Log("Photo read into a byte list.");
        } else {
            Debug.LogError("Failed to capture the photo.");
        }

        photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);
    }


}
