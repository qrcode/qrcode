﻿using UnityEngine;
using System.Collections;

#if !UNITY_EDITOR
using System.Threading.Tasks;
#endif



public class CameraTaskScript : MonoBehaviour {

#if !UNITY_EDITOR
    async Task Start () {
        await DoSomethingLong();
    }
#else
    void Start() {
    }
#endif

    // Update is called once per frame
    void Update () {

#if !UNITY_EDITOR
#endif

    }

#if !UNITY_EDITOR
    async Task DoSomethingLong()
    {
        System.Diagnostics.Debug.WriteLine(">>>>> Very long task started.");
        await Task.Delay(3000);
        System.Diagnostics.Debug.WriteLine("<<<<< Very long task ended.");
    }
#endif

}
