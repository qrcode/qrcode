﻿using UnityEngine;
using System.Collections;

public class CameraThreadScript : MonoBehaviour {

    MyThreadedJob myJob;

    // Use this for initialization
    void Start () {
        myJob = new MyThreadedJob();

        myJob.InData = new Vector3[10];
        myJob.Start(); // Don't touch any data in the job class after you called Start until IsDone is true

        // ???
        StartCoroutine("CheckForDone");
    }
    IEnumerator CheckForDone()
    {
        yield return StartCoroutine(myJob.WaitFor());
    }

    // Update is called once per frame
    void Update () {
        if (myJob != null) {
            if (myJob.Update()) {
                // Alternative to the OnFinished callback
                myJob = null;
            }
        }
    }
}
