﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public class WebCamPhotoCapture : MonoBehaviour {

    // TBD: Make this public ???
    private Renderer webCamRenderer;

    // Alternatively, use RawImage (in UI) ????
    public RawImage rawimage;
    // ....

    private WebCamDevice[] webCamDevices;
    private WebCamTexture webCamTexture;

    // temporary
    // private bool webCamJustStarted = false;
    private int webCamFrameCount = 0;

    void Awake()
    {
        webCamDevices = WebCamTexture.devices;
    }

    void Start () {
        // for debugging
        foreach (var device in webCamDevices) {
            Debug.Log("device = " + device.name);
        }

        // Renderer
        webCamRenderer = GetComponent<Renderer>();

        if (webCamDevices.Length > 0)
        {
            // ???
            // WebCamTexture webcam = new WebCamTexture("Integrated Webcam");
            // WebCamTexture webCamTexture = new WebCamTexture(webCamDevices[0].name);
            webCamTexture = new WebCamTexture();

			// cameraResolution.width = 1344;
			// cameraResolution.height = 756;
			// cameraResolution.refreshRate = 30;
			// webTex = new WebCamTexture(WebCamTexture.devices.First<WebCamDevice>().name, cameraResolution.width, cameraResolution.height, cameraResolution.refreshRate);

			// wc = new WebCamTexture ();
			// sp = new Texture2D (wc.width, wc.height);
			// GameObject.Find ("/Sphere").GetComponent<Renderer().material.mainTexture = sp;
			// sp.SetPixels(wc.GetPixels());
			// sp.Apply();

            webCamRenderer.material.mainTexture = webCamTexture;

            // Alternatively...
            // rawimage.texture = webCamTexture;
            // rawimage.material.mainTexture = webCamTexture;

            webCamTexture.Play();
            // webCamJustStarted = true;   // temporary
            webCamFrameCount = 1;
        }
    }

    // Update is called once per frame
    void Update () {
        // testing...
        // This does not work if we call TakePhoto() too early....
//	    if(webCamJustStarted)
//        {
//            webCamJustStarted = false;
//            StartCoroutine("TakePhoto");
//        }
        // testing ....
        if (webCamFrameCount > 0)
        {
            webCamFrameCount++;
            if(webCamFrameCount >= 100)
            {
                webCamFrameCount = 0;
                StartCoroutine("TakePhoto");
            }
        }
    }

    // Cf. http://docs.unity3d.com/ScriptReference/WebCamTexture.html
    IEnumerator TakePhoto()
    {
        Debug.Log("TakePhoto() called.");

        // NOTE - you almost certainly have to do this here:

        yield return new WaitForEndOfFrame();

        // it's a rare case where the Unity doco is pretty clear,
        // http://docs.unity3d.com/ScriptReference/WaitForEndOfFrame.html
        // be sure to scroll down to the SECOND long example on that doco page 

        Texture2D photo = new Texture2D(webCamTexture.width, webCamTexture.height);
        photo.SetPixels(webCamTexture.GetPixels());
        photo.Apply();

        //Encode to a PNG or JPG.
        // byte[] bytes = photo.EncodeToPNG();
        byte[] bytes = photo.EncodeToJPG();

        //Write out the PNG. Of course you have to substitute your_path for something sensible
        // File.WriteAllBytes(your_path + "photo.png", bytes);

        string filename = string.Format(@"WebCamImage{0}_n.jpg", Time.time);
        string filePath = System.IO.Path.Combine(Application.persistentDataPath, filename);
        Debug.Log("Saving webcam photo to " + filePath);
        File.WriteAllBytes(filePath, bytes);

    }


}
