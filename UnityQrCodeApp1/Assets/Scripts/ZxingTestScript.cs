﻿using UnityEngine;
using System;
using System.Collections;
#if !UNITY_EDITOR
using ZXing;
using Windows.UI.Xaml.Media.Imaging;
#endif

public class ZxingTestScript : MonoBehaviour {



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}



#if !UNITY_EDITOR

    private Result ScanBitmap(WriteableBitmap writeableBmp = null)
    {
        // ????
        if (writeableBmp == null) {
            return null;
        }

        var barcodeReader = new BarcodeReader {
            TryHarder = true,
            AutoRotate = true
        };
        // barcodeReader.Options = ....
        ZXing.Result result = null;
        try {
            result = barcodeReader.Decode(writeableBmp);
        } catch (Exception ex) {
            // What to do???
            System.Diagnostics.Debug.WriteLine("barcodeReader.Decode() failed. Error = {0}", ex.Message);
        }

        return result;
    }

    private BitmapImage ConvertTexture2DToBitmapImage(Texture2D texture2D)
    {
        byte[] textureData = new byte[4 * texture2D.width * texture2D.height];
        textureData = texture2D.GetRawTextureData();
        //ExtendedImage extendedImage = new ExtendedImage();
        //extendedImage.SetPixels(texture2D.Width, texture2D.Height, textureData);
        //JpegEncoder encoder = new JpegEncoder();
        //using (Stream picStream = new MemoryStream()) {
        //    encoder.Encode(extendedImage, picStream);
        //    picStream.Position = 0;
        //    BitmapImage bitmapImage = new BitmapImage();
        //    bitmapImage.SetSource(picStream);
        //    return bitmapImage;
        //}
        return null;
    }

#endif

}
