﻿using UnityEngine;
using System.Collections;
using UnityEngine.VR.WSA.WebCam;
using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;

#if !UNITY_EDITOR
using Windows.UI.Xaml.Media.Imaging;
using Windows.Storage.Streams;
using System.Threading.Tasks;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.UI.Core;
using Windows.Storage;
using Windows.Graphics.Imaging;
#endif


public class CameraBitmapScript : MonoBehaviour {



    // #if !UNITY_EDITOR
    // #endif

    PhotoCapture photoCaptureObject = null;
    //bool haveFolderPath = false;
    //StorageFolder picturesFolder;
    //string tempFilePathAndName;
    //string tempFileName;

    // Use this for initialization
    void Start()
    {
        //getFolderPath();
        //while (!haveFolderPath)
        //{
        //    Debug.Log("Waiting for folder path...");
        //}

        // DoPhotoCapture();


        // the interval should be long enough that the photocapture should be cleaned up before the next one starts.
        // InvokeRepeating("DoPhotoCapture", 2, 5.0F);

        // Apparently, coroutine is better than InvokeRepeating in terms of performance...
        StartCoroutine("PhotoCaptureCoroutine", 7.5f);
    }



    //async void getFolderPath()
    //{
    //    StorageLibrary myPictures = await Windows.Storage.StorageLibrary.GetLibraryAsync(Windows.Storage.KnownLibraryId.Pictures);
    //    picturesFolder = myPictures.SaveFolder;

    //    foreach(StorageFolder fodler in myPictures.Folders)
    //    {
    //        Debug.Log(fodler.Name);

    //    }

    //    Debug.Log("savePicturesFolder.Path is " + picturesFolder.Path);
    //    haveFolderPath = true;
    //}



    void DoPhotoCapture()
    {
        Debug.Log("About to call PhotoCapture.CreateAsync()");
        PhotoCapture.CreateAsync(false, OnPhotoCaptureCreated);
        Debug.Log("Called PhotoCapture.CreateAsync()");
    }
    IEnumerator PhotoCaptureCoroutine(float interval)
    {
        //yield return new WaitForSeconds(interval);

        //while (true) {
        //    Debug.Log("About to call PhotoCapture.CreateAsync()");
        //    PhotoCapture.CreateAsync(false, OnPhotoCaptureCreated);
        //    Debug.Log("Called PhotoCapture.CreateAsync()");

        //    yield return new WaitForSeconds(interval);
        //}

        // temporary
        // do once for now...
        // while (true) {
            yield return new WaitForSeconds(interval);

            //Debug.Log("About to call PhotoCapture.CreateAsync()");
            //PhotoCapture.CreateAsync(false, OnPhotoCaptureCreated);
            //Debug.Log("Called PhotoCapture.CreateAsync()");

            DoPhotoCapture();
            // yield return null;
            yield return new WaitForSeconds(2.5f);

            DoPhotoScan();
            // yield return null;


        // tbd:
        // do more processing, and yield.
        // etc...
        // }
    }



#if !UNITY_EDITOR
    async void OnPhotoCaptureCreated(PhotoCapture captureObject)
#else
    void OnPhotoCaptureCreated(PhotoCapture captureObject)
#endif
    {
        photoCaptureObject = captureObject;

        Resolution cameraResolution = PhotoCapture.SupportedResolutions.OrderByDescending((res) => res.width * res.height).First();

        CameraParameters c = new CameraParameters();
        c.hologramOpacity = 0.0f;
        c.cameraResolutionWidth = cameraResolution.width;
        c.cameraResolutionHeight = cameraResolution.height;
        c.pixelFormat = CapturePixelFormat.BGRA32;

        captureObject.StartPhotoModeAsync(c, false, OnPhotoModeStarted);

#if !UNITY_EDITOR
        System.Diagnostics.Debug.WriteLine(">>>>>> Long task started...");
        await Task.Delay(3000);
        System.Diagnostics.Debug.WriteLine("<<<<<< Long task ended...");
#endif

    }

    void OnStoppedPhotoMode(PhotoCapture.PhotoCaptureResult result)
    {
        photoCaptureObject.Dispose();
        photoCaptureObject = null;
    }



    private void OnPhotoModeStarted(PhotoCapture.PhotoCaptureResult result)
    {
        if (result.success) {
            try {
                // [1]
                //string filename = string.Format(@"CapturedImage{0}_n.jpg", Time.time);
                //string filePath = Path.GetFullPath(Path.Combine(Application.persistentDataPath, filename));
                //Debug.Log("Saving photo to " + filePath);
                // photoCaptureObject.TakePhotoAsync(filePath, PhotoCaptureFileOutputFormat.JPG, OnCapturedPhotoToDisk);

                // [2]
                // photoCaptureObject.TakePhotoAsync(OnCapturedPhotoToMemory1);
                // photoCaptureObject.TakePhotoAsync(OnCapturedPhotoToMemory2);

                // [3]
                // ?????
                // Does this work? (OnCapturedPhotoToMemory3 is an async method)
                // -->
                // Getting Platform.WrongThreadException related to threading...
                // Does it have anything to do with async call????
                // .....
                // photoCaptureObject.TakePhotoAsync(OnCapturedPhotoToMemory3);

                // [4]
                photoCaptureObject.TakePhotoAsync(OnCapturedPhotoToBitmapFile);
                photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);
                // ....


            } catch (System.ArgumentException e) {
                Debug.LogError("System.ArgumentException:\n" + e.Message);
            }
        } else {
            Debug.LogError("Unable to start photo mode!");
        }
    }

    void OnCapturedPhotoToDisk(PhotoCapture.PhotoCaptureResult result)
    {
        if (result.success) {
            Debug.Log("Saved Photo to disk!");
            photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);

            //Debug.Log("moving "+tempFilePathAndName+" to " + picturesFolder.Path + "\\Camera Roll\\" + tempFileName);
            //File.Move(tempFilePathAndName, picturesFolder.Path + "\\Camera Roll\\" + tempFileName);
        } else {
            Debug.Log("Failed to save Photo to disk " + result.hResult + " " + result.resultType.ToString());
        }
    }

    void OnCapturedPhotoToMemory1(PhotoCapture.PhotoCaptureResult result, PhotoCaptureFrame photoCaptureFrame)
    {
        if (result.success) {
            // Create our Texture2D for use and set the correct resolution
            Resolution cameraResolution = PhotoCapture.SupportedResolutions.OrderByDescending((res) => res.width * res.height).First();
            Texture2D targetTexture = new Texture2D(cameraResolution.width, cameraResolution.height);
            // Copy the raw image data into our target texture
            photoCaptureFrame.UploadImageDataToTexture(targetTexture);
            // Do as we wish with the texture such as apply it to a material, etc.

            Debug.Log("Photo saved to memory.");
        } else {
            Debug.LogError("Failed to save the photo to memory.");
        }

        // Clean up
        photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);
    }

    void OnCapturedPhotoToMemory2(PhotoCapture.PhotoCaptureResult result, PhotoCaptureFrame photoCaptureFrame)
    {
        if (result.success) {
            List<byte> imageBufferList = new List<byte>();
            // Copy the raw IMFMediaBuffer data into our empty byte list.
            photoCaptureFrame.CopyRawImageDataIntoBuffer(imageBufferList);

            // In this example, we captured the image using the BGRA32 format.
            // So our stride will be 4 since we have a byte for each rgba channel.
            // The raw image data will also be flipped so we access our pixel data
            // in the reverse order.
            int stride = 4;
            float denominator = 1.0f / 255.0f;
            List<Color> colorArray = new List<Color>();
            for (int i = imageBufferList.Count - 1; i >= 0; i -= stride) {
                float a = (int)(imageBufferList[i - 0]) * denominator;
                float r = (int)(imageBufferList[i - 1]) * denominator;
                float g = (int)(imageBufferList[i - 2]) * denominator;
                float b = (int)(imageBufferList[i - 3]) * denominator;

                colorArray.Add(new Color(r, g, b, a));
            }
            // Now we could do something with the array such as texture.SetPixels() or run image processing on the list

            Debug.Log("Photo read into a byte list.");
        } else {
            Debug.LogError("Failed to capture the photo.");
        }

        photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);
    }



    // https://msdn.microsoft.com/en-us/library/windows/apps/windows.ui.xaml.media.imaging.writeablebitmap.aspx

    // ???
    private void OnCapturedPhotoToMemory3(PhotoCapture.PhotoCaptureResult result, PhotoCaptureFrame photoCaptureFrame)
    {
        if (result.success) {

            // var pixelFormat = photoCaptureFrame.pixelFormat;
            // ???

            List<byte> imageBufferList = new List<byte>();
            // Copy the raw IMFMediaBuffer data into our empty byte list.
            photoCaptureFrame.CopyRawImageDataIntoBuffer(imageBufferList);

            byte[] bytes = imageBufferList.ToArray();

            //// In this example, we captured the image using the BGRA32 format.
            //// So our stride will be 4 since we have a byte for each rgba channel.
            //// The raw image data will also be flipped so we access our pixel data
            //// in the reverse order.
            //int stride = 4;
            //float denominator = 1.0f / 255.0f;
            //List<Color> colorArray = new List<Color>();
            //for (int i = imageBufferList.Count - 1; i >= 0; i -= stride) {
            //    float a = (int)(imageBufferList[i - 0]) * denominator;
            //    float r = (int)(imageBufferList[i - 1]) * denominator;
            //    float g = (int)(imageBufferList[i - 2]) * denominator;
            //    float b = (int)(imageBufferList[i - 3]) * denominator;

            //    colorArray.Add(new Color(r, g, b, a));
            //}
            //// Now we could do something with the array such as texture.SetPixels() or run image processing on the list

            Debug.Log("Photo read into a byte list.");
            //foreach (var b in bytes) {
            //    Debug.Log($">> b = {b}.");
            //}

#if !UNITY_EDITOR

            //Resolution cameraResolution = PhotoCapture.SupportedResolutions.OrderByDescending((res) => res.width * res.height).First();
            //int width = cameraResolution.width;
            //int height = cameraResolution.height;

            //WriteableBitmap bitmap = new WriteableBitmap(width, height);

            //var t = Task.Run(async () => {
            //    using (var stream = await ConvertToStreamAsync(bytes)) {
            //        await bitmap.SetSourceAsync(stream);
            //    }
            //    Debug.Log(">>>>>> Writeable bitmap created.");

            //    //System.Diagnostics.Debug.WriteLine("====== bitmap created ===== ");
            //    //var pix = bitmap.PixelBuffer.ToArray();
            //    //foreach (var p in pix) {
            //    //    System.Diagnostics.Debug.WriteLine($">> p = {p}.");
            //    //}


            //    string bmpFilename = string.Format(@"BitmapImage{0}_n.png", Time.time);
            //    string bmpFilepath = Path.Combine(Application.persistentDataPath, bmpFilename);
            //    Debug.Log("Saving writeable bitmap to " + bmpFilepath);

            //    var file = await ApplicationData.Current.TemporaryFolder.CreateFileAsync(bmpFilename, CreationCollisionOption.GenerateUniqueName);
            //    using (IRandomAccessStream stream = await file.OpenAsync(FileAccessMode.ReadWrite)) {
            //        BitmapEncoder encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.PngEncoderId, stream);
            //        Stream pixelStream = bitmap.PixelBuffer.AsStream();
            //        byte[] pixelBytes = new byte[pixelStream.Length];
            //        await pixelStream.ReadAsync(pixelBytes, 0, pixelBytes.Length);

            //        encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore,
            //                            (uint)bitmap.PixelWidth,
            //                            (uint)bitmap.PixelHeight,
            //                            96.0,
            //                            96.0,
            //                            pixelBytes);
            //        await encoder.FlushAsync();
            //    }


            //    return bitmap;
            //});


            Debug.Log("??? writeableBitmap created. ???");

            //WriteableBitmap writeableBitmap = t.Result;

            //var pixels = writeableBitmap.PixelBuffer.ToArray();
            //foreach (var p in pixels) {
            //    Debug.Log($">> p = {p}.");
            //}


#endif



        } else {
            Debug.LogError("Failed to capture the photo.");
        }


        // ...
        Debug.LogError("About to stop the photomode....");
        photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);
        Debug.LogError("Photomode stopped!!!!");
    }



    // TBD:
    // Is there a better way???
    // This seems to be the only way I can create a writeablebitmap...
    // .....
    void OnCapturedPhotoToBitmapFile(PhotoCapture.PhotoCaptureResult result, PhotoCaptureFrame photoCaptureFrame)
    {
        if (result.success) {
            // Create our Texture2D for use and set the correct resolution
            Resolution cameraResolution = PhotoCapture.SupportedResolutions.OrderByDescending((res) => res.width * res.height).First();
            var width = cameraResolution.width;
            var height = cameraResolution.height;
            Texture2D targetTexture = new Texture2D(width, height);
            // Copy the raw image data into our target texture
            photoCaptureFrame.UploadImageDataToTexture(targetTexture);
            // Do as we wish with the texture such as apply it to a material, etc.

            Debug.Log("Photo saved to memory.");


            string bmpFilename = string.Format(@"TextureImage{0}_n.png", Time.time);
            string bmpFilepath = Path.GetFullPath(Path.Combine(Application.persistentDataPath, bmpFilename));
            Debug.Log("Saving texture image to " + bmpFilepath);

            File.WriteAllBytes(bmpFilepath, targetTexture.EncodeToPNG());
            // if success...
            mostRecentPhotoBitmapFilePath = bmpFilepath;
            mostRecentPhotoBitmapWidth = width;
            mostRecentPhotoBitmapHeight = height;
            // else ???
            // ...



        } else {
            Debug.LogError("Failed to save the photo to memory.");
        }

        // Clean up
        // photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);
    }


    // temporary
    private string mostRecentPhotoBitmapFilePath = null;
    private int mostRecentPhotoBitmapWidth = 1;  // ??
    private int mostRecentPhotoBitmapHeight = 1;  // ??
    // temporary
    void DoPhotoScan()
    {
        Debug.Log(">>>>> DoPhotoScan()");
#if !UNITY_EDITOR
        this.SendMessage("ProcessPhotoScanAsync");
#endif
    }
#if !UNITY_EDITOR
    private async Task ProcessPhotoScanAsync()
    {
        Debug.Log($">>>>> mostRecentPhotoBitmapFilePath = {mostRecentPhotoBitmapFilePath}.");
        if (mostRecentPhotoBitmapFilePath != null) {

            // temporary
            var photoFielPath = mostRecentPhotoBitmapFilePath;
            // ?????

            var photoStorageFile = await StorageFile.GetFileFromPathAsync(photoFielPath);
            Debug.Log($"photoStorageFile for path = {photoFielPath}.");

            var stream = await photoStorageFile.OpenReadAsync();
            Debug.Log("photoStorageFile stream read.");


            // TBD:
            // new WriteableBitmap() throws exception for some reason....
            // why ????????


            Debug.Log(">>>>> About to call new WriteableBitmap().");
            //// initialize with 1,1 to get the current size of the image
            //var writeableBmp = new WriteableBitmap(1, 1);
            //// ...
            //writeableBmp.SetSource(stream);
            //// and create it again because otherwise the WB isn't fully initialized and decoding
            //// results in a IndexOutOfRange
            //writeableBmp = new WriteableBitmap(writeableBmp.PixelWidth, writeableBmp.PixelHeight);
            //stream.Seek(0);
            //writeableBmp.SetSource(stream);


            var writeableBmp = new WriteableBitmap(mostRecentPhotoBitmapWidth, mostRecentPhotoBitmapHeight);
            writeableBmp.SetSource(stream);
            Debug.Log(">>>>> writeableBmp constructed.");



            // do something here with writeableBmp..
            // ....


            // Just testing..

            string bmpFilename = string.Format(@"BitmapImage{0}_n.png", Time.time);
            string bmpFilepath = Path.GetFullPath(Path.Combine(Application.persistentDataPath, bmpFilename));
            Debug.Log("Saving writeable bitmap to " + bmpFilepath);

            var file = await ApplicationData.Current.TemporaryFolder.CreateFileAsync(bmpFilename, CreationCollisionOption.GenerateUniqueName);
            using (IRandomAccessStream bmpStream = await file.OpenAsync(FileAccessMode.ReadWrite)) {
                BitmapEncoder encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.PngEncoderId, bmpStream);
                Stream pixelStream = writeableBmp.PixelBuffer.AsStream();
                byte[] pixelBytes = new byte[pixelStream.Length];
                await pixelStream.ReadAsync(pixelBytes, 0, pixelBytes.Length);

                encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore,
                                    (uint)writeableBmp.PixelWidth,
                                    (uint)writeableBmp.PixelHeight,
                                    96.0,
                                    96.0,
                                    pixelBytes);
                await encoder.FlushAsync();
            }

            // Just testing..



            // temporary
            // await photoStorageFile.DeleteAsync(StorageDeleteOption.PermanentDelete);
        }
    }
#endif


#if !UNITY_EDITOR
    internal static async Task<InMemoryRandomAccessStream> ConvertToStreamAsync(byte[] arr)
    {
        InMemoryRandomAccessStream randomAccessStream = new InMemoryRandomAccessStream();
        await randomAccessStream.WriteAsync(arr.AsBuffer());
        randomAccessStream.Seek(0); // Just to be sure.
                                    // I don't think you need to flush here, but if it doesn't work, give it a try.
        return randomAccessStream;
    }
#endif

}
